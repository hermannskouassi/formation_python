#Affiche le message de bienvenue
print ("Bienvenue dans le monde des calculs")

#Recevoir la valeur de la variable Nb1 de l'utilisateur
Nb1 = input("Saisir le premier nombre : ")

#Recevoir la valeur de la variable Nb2 de l'utilisateur
Nb2 = input("Saisir le deuxieme nombre : ")

#Associer les variables Nb1 et Nb2
Total = Nb1 + Nb2

#Affiche la valeur de la variable Total 
print ("La somme des deux nombres est : ", Total)

#Definir une variable de type entier
n = 5

#Affichage de la variable
print ("La valeur de n est:", n)