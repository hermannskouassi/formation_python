#Affiche le message de bienvenue
print("Akwaba Python Calculator")

#Creation d'une fonction somme avec deux variables x et y
def somme(x,y):
	return x+y

#Creation d'une fonction main qui fait la somme de x et y en utilsat la function somme 
def main():
	print("Faire la somme x et y")
	x = int(input("Premier chiffre est:"))
	y = int(input("Deuxieme chiffre est:"))
	resultat = somme (x,y)
	print("La somme de",x ,"et",y,"est:",resultat)

#Permet d'executer la fonction main, 
#car lorsque que je commente ces lignes seulement le msg de bienvenue est affiché
if __name__=="__main__":
	main()