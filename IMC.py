#Calculer mon IMC

poids = int(input("Indiquer votre poids en kg: "))
taille = float(input("Indiquer votre taille en m: "))

def calcul(poids, taille):
	imc = poids / taille**2
	return imc 

def diagnostic(imc):
	if (imc > 35) :
 		print("L'interpretation de votre IMC (", imc ,"kg/m²) est: OBESITE SEVERE.")
	elif (imc > 30) and (imc < 34.9):
 		print("L'interpretation de votre IMC (", imc ,"kg/m²) est: OBESITE.")
	elif (imc > 25) and (imc < 29.9):
 		print("L'interpretation de votre IMC (", imc ,"kg/m²) est: SURPOIDS.")
	elif (imc > 18.5) and (imc < 24.9):
 		print("L'interpretation de votre IMC (", imc ,"kg/m²) est: CORPULENCE NORMALE.")
	elif imc < 18.5 :
 		print("L'interpretation de votre IMC (", imc ,"kg/m²) est: MAIGRE.")

def main():
	a = calcul(poids,taille)
	diagnostic(a)
	
if __name__ == '__main__':
	main()