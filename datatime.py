#!/usr/bin/python

#import date class
from datetime import date

#initializong contructor and passing argument
my_date = date(2019, 11, 23)

print("Date passed as arguement is", my_date)

#calling the the today
today = date.today()

print("Today's date is", today)

#printing date's components 
print("Date components", today.year, today.month, today.day) 
print("#------------------\n")
#import time class
from datetime import time

# calling the constructor 
my_time = time(13, 24, 56) 

print("Entered time", my_time)

# calling constructor with 1 argument 
my_time = time(minute = 12) 
print("\nTime with one argument", my_time)

# Calling constructor with  
# 0 argument 
my_time = time() 
print("\nTime without argument", my_time) 