from tkinter import*
#from math import *
import math

class calculate():

    def __init__(self):
        self.root = Tk()
        self.root.title("Calculator")
        #self.root.geometry("370x220")

        self.resultwindow = Entry(self.root)
        self.resultwindow.grid(row=0,column=0,columnspan=6)
        self.resultwindow.config(font=("Arial", 18))
        self.resultwindow.config(background='#DAF7A6')
        self.resultwindow.focus_set()  # Sets focus on the input text area

        # Buttons
        self.button_open = Button(self.root, text="(", width=3, command=lambda: self.ins('('))
        self.button_open.grid(row=1, column=0, padx=3, pady=3)
        self.button_open.config(font=("Arial", 18))

        self.button_close = Button(self.root, text=")", width=3, command=lambda: self.ins(')'))
        self.button_close.grid(row=1, column=1, padx=3, pady=3)
        self.button_close.config(font=("Arial", 18))

        self.buttoncancel = Button(self.root, text="C", width=3, command=lambda: self.cancel())
        self.buttoncancel.grid(row=1, column=2, padx=3, pady=3)
        self.buttoncancel.config(font=("Arial", 18))

        self.buttondeleteall = Button(self.root, text="<-", width=3, command=lambda: self.delete_all())
        self.buttondeleteall.grid(row=0, column=5, padx=3, pady=3)
        self.buttondeleteall.config(font=("Arial", 18))
        
        self.button_one = Button(self.root, text="1", width=3, command=lambda:self.ins('1'))
        self.button_one.grid(row=2,column=0, padx=3, pady=3)
        self.button_one.config(font=("Arial", 18))
        self.button_one.config(background='#ff886a')

        self.button_nine = Button(self.root, text="9", width=3, command=lambda:self.ins('9'))
        self.button_nine.grid(row=2, column=2, padx=3, pady=3)
        self.button_nine.config(font=("Arial", 18))

        self.button_eight = Button(self.root, text="8", width=3, command=lambda:self.ins('8'))
        self.button_eight.grid(row=2, column=1, padx=3, pady=3)
        self.button_eight.config(font=("Arial", 18))

        self.button_seven = Button(self.root, text="7", width=3, command=lambda:self.ins('7'))
        self.button_seven.grid(row=2, column=0, padx=3, pady=3)
        self.button_seven.config(font=("Arial", 18))

        self.button_six = Button(self.root, text="6", width=3, command=lambda:self.ins('6'))
        self.button_six.grid(row=3, column=2, padx=3, pady=3)
        self.button_six.config(font=("Arial", 18))

        self.button_five = Button(self.root, text="5", width=3, command=lambda:self.ins('5'))
        self.button_five.grid(row=3, column=1, padx=3, pady=3)
        self.button_five.config(font=("Arial", 18))

        self.button_four = Button(self.root, text="4", width=3, command=lambda:self.ins('4'))
        self.button_four.grid(row=3, column=0, padx=3, pady=3)
        self.button_four.config(font=("Arial", 18))

        self.button_three = Button(self.root, text="3", width=3, command=lambda:self.ins('3'))
        self.button_three.grid(row=4, column=2, padx=3, pady=3)
        self.button_three.config(font=("Arial", 18))

        self.button_two = Button(self.root, text="2", width=3, command=lambda:self.ins('2'))
        self.button_two.grid(row=4, column=1, padx=3, pady=3)
        self.button_two.config(font=("Arial", 18))

        self.button_one = Button(self.root, text="1", width=3, command=lambda: self.ins('1'))
        self.button_one.grid(row=4, column=0, padx=3, pady=3)
        self.button_one.config(font=("Arial", 18))

        self.button_zero = Button(self.root, text="0", width=3, command=lambda: self.ins('0'))
        self.button_zero.grid(row=5, column=0, padx=3, pady=3)
        self.button_zero.config(font=("Arial", 18))

        self.button_dot = Button(self.root, text=".", width=3, command=lambda: self.ins('.'))
        self.button_dot.grid(row=5, column=1, padx=3, pady=3)
        self.button_dot.config(font=("Arial", 18))

        # Operations Buttons

        self.buttonplus = Button(self.root, text="+", width=3, command=lambda:self.ins('+'))
        self.buttonplus.grid(row=2, column=4, padx=3, pady=3)
        self.buttonplus.config(font=("Arial", 18))

        self.buttonminus = Button(self.root, text="-", width=3, command=lambda:self.ins('-'))
        self.buttonminus.grid(row=2, column=5, padx=3, pady=3)
        self.buttonminus.config(font=("Arial", 18))

        self.button_cos = Button(self.root, text="cos", width=3, command=lambda:self.calc_cos())
        self.button_cos.grid(row=2, column=3, padx=3, pady=3)
        self.button_cos.config(font=("Arial", 18))

        self.buttondivide = Button(self.root, text="/", width=3, command=lambda:self.ins('/'))
        self.buttondivide.grid(row=3, column=4, padx=3, pady=3)
        self.buttondivide.config(font=("Arial", 18))

        self.buttonmultiply = Button(self.root, text="*", width=3, command=lambda:self.ins('*'))
        self.buttonmultiply.grid(row=3, column=5, padx=3, pady=3)
        self.buttonmultiply.config(font=("Arial", 18))

        self.button_sin = Button(self.root, text="sin", width=3, command=lambda:self.calc_sin())
        self.button_sin.grid(row=3, column=3, padx=3, pady=3)
        self.button_sin.config(font=("Arial", 18))

        self.button_square = Button(self.root, text="x²", width=3, command=lambda:self.square())
        self.button_square.grid(row=4, column=4, padx=3, pady=3)
        self.button_square.config(font=("Arial", 18))

        self.button_rtsquare = Button(self.root, text="sqrt", width=3, command=lambda:self.squareroot())
        self.button_rtsquare.grid(row=4, column=5, padx=3, pady=3)
        self.button_rtsquare.config(font=("Arial", 18))

        self.button_tan = Button(self.root, text="tan", width=3, command=lambda:self.calc_tan())
        self.button_tan.grid(row=4, column=3, padx=3, pady=3)
        self.button_tan.config(font=("Arial", 18))

        self.button_exp = Button(self.root, text="x^y", width=3, command=lambda:self.calc_exp())
        self.button_exp.grid(row=5, column=2, padx=3, pady=3)
        self.button_exp.config(font=("Arial", 18))

        self.buttonresult = Button(self.root, text="=", width=6, command=lambda:self.calculate())
        self.buttonresult.grid(row=5, column=5, padx=3, pady=2, columnspan=2)
        self.buttonresult.config(font=("Arial", 18))
        self.buttonresult.config(background='#ff886a')

        self.root.mainloop()

    def ins(self,val):
        self.resultwindow.insert(END, val)

    def cancel(self):
        self.resultwindow.delete(0, 'end')

    def delete_all(self):
        x = self.resultwindow.get()
        self.resultwindow.delete(0, 'end')
        y = x[:-1]
        self.resultwindow.insert(0, y)

    def calculate(self):
        x = self.resultwindow.get()
        answer = eval(x)
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def squareroot(self): 
        
        x = self.resultwindow.get()
        answer = math.sqrt(float(x)) 
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def square(self): 
        x = self.resultwindow.get()
        answer = float(x)**2
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def calc_cos(self): 
        x = self.resultwindow.get()
        answer = math.cos(float(x))
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def calc_sin(self): 
        x = self.resultwindow.get()
        answer = math.sin(float(x))
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def calc_tan(self): 
        x = self.resultwindow.get()
        answer = math.tan(float(x))
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert(0, answer)

    def calc_exp(self): 
        x = self.resultwindow.get()
        answer = math.exp(float(x,y))
        self.resultwindow.delete(0, 'end')
        self.resultwindow.insert((0, answer))

if __name__ == "__main__":
    calculate()