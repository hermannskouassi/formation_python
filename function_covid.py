#Import module 
from covid import Covid 

#variable
covid = Covid()

#Define a function
def  displayMessage():
	print("--------------------------")
	print("Akwaba Abidjan Python: Information about Covid19 . God bless You !")
	print("--------------------------")
	
def getStatusByCountryName(CountryName):
	country_cases = covid.get_status_by_country_name(CountryName)

	print("Covid19 Status :" , CountryName,country_cases)
	print("** ** ** ** ** ")

#def displayAllCovidData():
#	allCovidData = covid.get_data()
#	print("Covid data/Countries :" ,covid.get_data())

def main():
	displayMessage()
	getStatusByCountryName("Cote d'Ivoire")

if __name__ == '__main__':
	main()
