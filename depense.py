#Ce script calcule les depenses en cigarette en fonction du nombre de cigarette acheter sur une periode donnée

def cigdepense():
	#variable nombre de cigarette fumée par jour
	nb_cig_jr = int(input("Entrer le nombre de cigarette fumée par jour: "))

	#Variable Prix d'une cigarette
	prix = int(input("Combien coûte une cigarette? "))

	#Recuper la periode en mois 
	periode_user = int(input("Calculer votre consomation sur la periode de (en mois): "))
	
	#Variable nombre de jour dans 1 mois
	mois = int(30)	

	#Convertir la periode en jour
	periode_reel = periode_user * mois

	#Calculer le montant depenser
	depense = nb_cig_jr * prix * periode_reel
	print("Votre consomation sur la periode de", periode_user, "est de:")
	return depense

def main():
	result = cigdepense()
	print(result)

if __name__ == '__main__':
	main()